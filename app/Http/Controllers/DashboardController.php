<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;
use DB;

class DashboardController extends Controller
{
    public function index()
    {
        return view('adminpage.dashboardadmin');
    }
}
