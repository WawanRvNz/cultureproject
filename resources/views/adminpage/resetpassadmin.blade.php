<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>.:: Administrator System | Reset Password ::. </title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('template/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('template/admin/plugins/iCheck/square/blue.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" type="text/css" href="{{ asset('template/dist/sweetalert.css') }}">
</head>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>Indonesia</b> Culture</a>
  </div>
  <div class="login-box-body">
    <p align="center"><img src="{{ asset('template/admin/dist/img/indo.jpg') }}" width="150" height="150"></p>
    <p class="login-box-msg">Reset Your Password</p>
    @if (Session::has('after_login'))
      <div class="row">
          <div class="col-md-12">
              <div class="alert alert-dismissible alert-{{ Session::get('after_login.alert') }}">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ Session::get('after_login.title') }}</strong>
                  <a href="javascript:void(0)" class="alert-link">{{ Session::get('after_login.text-1') }}</a> {{ Session::get('after_login.text-2') }}
              </div>
          </div>
      </div>
    @endif
    <form action="" method="post">
      {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Enter Your Email">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-7">
          <div class="checkbox icheck"><a href="{{ Route('loginadmin') }}">Sign In</a></div>
        </div>
        <div class="col-xs-5">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
        </div>
      </div>
    </form>
    <br>
  </div>
  <p align="center" style="margin-top:10px; color: #607d8b;"> Copyright © 2017 Indonesia Culture 2017.<br> All rights reserved. </p>
</div>

<script src="{{ asset('template/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('template/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/admin/plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  });
</script>
<script>
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 2000);
</script>
@include('sweet::alert')
</body>
</html>
