<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// ==============================================BACKEND ADMINISTRATOR SECTION ========================================================
Route::get('panel/admin/login', 'LoginadminController@login')->name('loginadmin');
Route::get('panel/admin/reset/password', 'LoginadminController@reset')->name('resetpasswordadmin');

Route::get('panel/admin/dashboard', 'DashboardController@index')->name('dashboardadmin');
